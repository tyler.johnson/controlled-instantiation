#Controlled Instantiation

This is an example of a class that will only instantiate one instance per instance parameter

### Run
ctest --build-and-test . build --build-generator "Ninja" --build-noclean --test-command controlled-instantiation
