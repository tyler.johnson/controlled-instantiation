#include "controlled-instantiation.h"

using std::string;
using std::set;
using std::runtime_error;

set<string> ControlledInstantiation::instance_names{};

ControlledInstantiation::ControlledInstantiation(string name)
  : instance_name(name) {
  if (auto [iter, succ] = instance_names.insert(name); !succ) {
    throw runtime_error("Already instantiated with " + name);
  }
}
