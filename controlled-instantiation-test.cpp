#include "gtest/gtest.h"
#include "controlled-instantiation.h"

using std::runtime_error;

TEST(ControlledInstantiationTest, ThrowsWhenInstantiatedWithSameParam) {
  ASSERT_THROW({
    ControlledInstantiation test("test");
    ControlledInstantiation test2("test");
  }, runtime_error);
}

TEST(ControlledInstantiationTest, DoesNotThrowWhenInstantiatedWithDifferentParams) {
  ASSERT_NO_THROW({
    ControlledInstantiation test("test-one");
    ControlledInstantiation test2("test-two");
  });
}
