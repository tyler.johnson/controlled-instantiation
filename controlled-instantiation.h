#ifndef CONTROLLED_INSTANTIATION_H
#define CONTROLLED_INSTANTIATION_H

#include <string>
#include <set>
#include <stdexcept>

class ControlledInstantiation final {
  public:
  ControlledInstantiation(std::string name);

  private:
  std::string instance_name;
  static std::set<std::string> instance_names;
};
#endif
